# Blueprint Maker (extension pour Inkscape)

## Synopsis

*Blueprint Maker* est une extension pour le logiciel *Inkscape* qui transforme votre fichier en 2 couleurs: une pour le fond et une autre pour les traits.  
Cela donnera à votre travail une apparence similaire à celle d'un « plan ». Comme un plan technique ou d'architecte par exemple.  
L'épaisseur des traits sera constante pour toutes les formes ; et l'intérieur de ces formes sera vide.  
Soit vous agissez sur une sélection précise, soit, lorsque rien n'est sélectionné, tout sera affecté.

Actuellement, vous disposez de plusieurs palettes utilisables:
 + Blueprint: fond bleu et trait blanc
 + Screen: fond noir et trait blanc
 + Paper: fond blanc et trait noir
 + Laser: fond blanc et trait rouge (R=255,G=0,B=0)

Vous pouvez régler l'épaisseur du trait et l'unité utilisée.

**Attention: Avant de l'utiliser, n'oubliez pas de sauvegarder votre travail car les modifications seront définitives !**


## Screenshots

![Blueprint Maker UI](https://framagit.org/SxncR0OR/inkscape-extension-blueprint/raw/master/blueprint-maker-ui.png "Blueprint Maker UI")


## Exemple d'utilisation

Exemple de modification avec une image originale (à gauche) et les 3 types d'effet disponibles (à droite):

| Image originale | Blueprint | Screen | Paper |
| :---: | :---: | :---: | :---: |
| ![Blueprint Maker eg Original](https://framagit.org/SxncR0OR/inkscape-extension-blueprint/raw/master/blueprint-maker-eg-original-s320q85.jpg "Blueprint Maker eg Original") | ![Blueprint Maker eg Blueprint](https://framagit.org/SxncR0OR/inkscape-extension-blueprint/raw/master/blueprint-maker-eg-blueprint-s320q85.jpg "Blueprint Maker eg Blueprint") | ![Blueprint Maker eg Screen](https://framagit.org/SxncR0OR/inkscape-extension-blueprint/raw/master/blueprint-maker-eg-screen-s320q85.jpg "Blueprint Maker eg Screen") | ![Blueprint Maker eg Paper](https://framagit.org/SxncR0OR/inkscape-extension-blueprint/raw/master/blueprint-maker-eg-paper-s320q85.jpg "Blueprint Maker eg Paper") |


## Installation

L'installation se résume à copier les fichiers `blueprint-maker.inx` et `blueprint-maker.py` dans le répertoire des extensions d'Inkscape...  
L'emplacement de ce répertoire varie selon le système d'exploitation utilisé.

### Sous Linux

 + Copiez les fichiers `blueprint-maker.inx` et `blueprint-maker.py` dans le répertoire des extensions d'Inkscape: `~/.config/inkscape/extensions`.
	Le chemin complet étant: `/home/votre-nom-dutilisateur-ici/.config/inkscape/extensions`
    Bien entendu, remplacez *votre-nom-dutilisateur-ici* par le nom adéquat.
 + Redémarez Inkscape.
 + Vous trouverez l'extension ajoutée dans le menu: `Extensions > My Toolbox > Blueprint Maker...`

### Sous Windows

 + Copiez les fichiers `blueprint-maker.inx` et `blueprint-maker.py` dans le répertoire des extensions d'Inkscape. Par exemple, si chez vous Inkscape est installé dans `C:\Program Files\Inkscape` alors le répertoire des extensions sera: `C:\Program Files\Inkscape\share\extensions`.
 + Redémarez Inkscape.
 + Vous trouverez l'extension ajoutée dans le menu: `Extensions > My Toolbox > Blueprint Maker...`


## Contact

<blueprintmaker.oliviervandecasteele@xoxy.net>


## Licence

Blueprint Maker est distribué sous licence:  
[GNU Affero General Public License version 3 (AGPL v3)](https://www.gnu.org/licenses/agpl-3.0.en.html)

---

Blueprint Maker is an extension for Inkscape allowing to create a blueprint effect.  
Copyright (C) 2016 Olivier D.V. Vandecasteele  

This program is free software: you can redistribute it and/or modify  
it under the terms of the GNU Affero General Public License as  
published by the Free Software Foundation, either version 3 of the  
License, or (at your option) any later version.  

This program is distributed in the hope that it will be useful,  
but WITHOUT ANY WARRANTY; without even the implied warranty of  
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
GNU Affero General Public License for more details.  

You should have received a copy of the GNU Affero General Public License  
along with this program.  If not, see <http://www.gnu.org/licenses/>.
